require 'open-uri'

result = []

doc = Nokogiri::HTML(open("https://austin.bcycle.com/station-locations")) # loading html of the url

map_script = doc.xpath('//div[@id="C017_controlContentContainer"]//script').last.to_html # searching google map script

ms = map_script.match(/function LoadKiosks(.*?)\}/m).to_s # searching LoadKioks function of the script

unit_arr = ms.scan(/var icon(.*?)markers.push/m) # making array of each map pin

unit_arr.each do |unit| # unit is each map point block
  # initialize the params
  lat = nil
  lng = nil
  location = nil
  address = nil
  city = nil
  state = nil
  bikes_avail_count = nil
  docks_avail_count = nil

  # parsing each line of the map point block
  unit.first.split("\n").each do |line|
    # parsing Lat and Lng params
  	if line.match(/google.maps.LatLng/).present? # Eg: var point = new google.maps.LatLng(30.26408, -97.74355);
      # finding position of the '(' and ')''
      s_b = line.index("(") + 1
      e_b = line.index(")") - 1

      # finding Lat and Lng
      arr = line[s_b..e_b].split(",")
      lat = arr.first.to_f
      lng = arr.last.to_f
    end

    # parsing location and count params
    if line.match(/createMarker/).present? # Eg: var marker = new createMarker(point, "<div class= .... </div>", icon, back, false);
      # finding "..." part
      str = line.match(/\"(.*?)\"/) 
      # converting the string to html with nokogiri
      html = Nokogiri::HTML(str[1])
      # finding location, address, city and state from the converted html
      location = html.xpath('//div[@class="location"]').children[0].inner_text
      address = html.xpath('//div[@class="location"]').children[2].inner_text
      city = html.xpath('//div[@class="location"]').children[4].inner_text
      state = html.xpath('//div[@class="location"]').children[6].inner_text

      # finding bikes available count and docks available count from the converted html
      bikes_avail_count = html.xpath('//div[@class="avail"]').children[1].inner_text
      docks_avail_count = html.xpath('//div[@class="avail"]').children[4].inner_text
  	end
  end

  result << [location, [address, city, state], bikes_avail_count.to_i, docks_avail_count.to_i, [lat, lng]]
end


