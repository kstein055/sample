require 'uri'
require 'net/http'
require 'net/https'
require 'nokogiri'
require 'open-uri'
require 'csv'
require 'json'

uri = URI.parse("https://maps.googleapis.com/maps/api/geocode/json")
args = {address: 'Austin,%20Texas'}
uri.query = URI.encode_www_form(args)
http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
request = Net::HTTP::Get.new(uri.request_uri)
response = http.request(request)
location = JSON.parse(response.body)

uri = URI.parse("https://www.spinlister.com/search")
https = Net::HTTP.new(uri.host, uri.port)
https.use_ssl = true
req = Net::HTTP::Post.new(uri.path, initheader = {'x-requested-with' => 'XMLHttpRequest'})
req.set_form_data('lat' => location['results'].first['geometry']['location']['lat'], 'lng' => location['results'].first['geometry']['location']['lng'], 'radius' => 13, 'type%5B%5D' => 'bike')
res = https.request(req)
result = JSON.parse(res.body)
arr = []
result.each do |r| 
 doc = Nokogiri::HTML(open("https://www.spinlister.com/rides/#{r['uri']}"))
 owner_name = doc.xpath("//div[@class='panel-container'][1]/h4[@class='clearfix']").text
 owner_name = owner_name.gsub("\n","")
 bikename = doc.xpath('//h1[@class="truncate"]').text   
 owner_profile_url = doc.xpath('//div[@class="panel-heading no-padding"]/a').map { |link| link['href']}
 owner_profile_url = "https://www.spinlister.com"+owner_profile_url.first
 bike_profile_url = "https://www.spinlister.com/rides/#{r['uri']}"
 owner_profile_img_url = doc.css(".panel-profile .profile img").map{|links| links['src']}
 owner_profile_img_url = owner_profile_img_url.first
 bike_img_url = doc.xpath('//a[@class="fancybox"]/img').map{|links| links['src']}
 bike_img_url = bike_img_url.first
 bike_type = doc.xpath('//div[@class="hidden-xs ride-details"]/h3').text
 bike_height = doc.xpath('//div[@class="hidden-xs ride-details"]/div[@class="lighter"]').text
 bike_height = bike_height.gsub("\n","")
 description = doc.xpath('//div[@class="js-collapser"]/p').text
 extras = doc.xpath('//div[@class="panel-blank-slate-message text-center"]/h4').text
 per_hour = doc.xpath("//div[@class='row pricing']/div[@class='col-xs-4 col-sm-4 col-md-4 col-lg-4'][1]/h4/span[1]").text
 per_hour = per_hour.split("$").last
 per_day = doc.xpath("//div[@class='row pricing']/div[@class='col-xs-4 col-sm-4 col-md-4 col-lg-4'][2]/h4/span[1]").text
 per_day = per_day.split("$").last
 per_week = doc.xpath("//div[@class='row pricing']/div[@class='col-xs-4 col-sm-4 col-md-4 col-lg-4'][3]/h4/span[1]").text
 per_week = per_week.split("$").last
 response_time = doc.xpath("//div[@class='panel-container'][4]/span[@class='lighter pull-right']").text
 last_seen = doc.xpath('//span[@class="pull-right rating-count"]').text
 count_reviews = doc.xpath('//div[@class="panel-container"]/span[@class="pull-right rating-count"]').text
 arr<<{:owner_name => owner_name, :bikename => bikename, :owner_profile_url => owner_profile_url, :bike_profile_url => bike_profile_url, :owner_profile_img_url => owner_profile_img_url, :bike_img_url => bike_img_url, :bike_type => bike_type, :bike_height => bike_height, :description => description, :extras => extras, :per_hour => per_hour, :per_day => per_day, :per_week => per_week, :response_time => response_time, :last_seen => last_seen, :count_reviews => count_reviews}
end
 
CSV.open("scrape.csv", "w") do |writer|
  writer << ["OwnerName","BikeName","OwnerProfileURL","BikeProfileURL","BikeImageURL","OwnerProfilePictureURL","BikeType","BikeHeight","InfoDescription","InfoExtras","$perHour","$perDay","$perWeek","$ResponseTime","$CountofReviews"]
  arr.each do |para|
    writer << [para[:owner_name],para[:bikename],para[:owner_profile_url],para[:bike_profile_url],para[:owner_profile_img_url],para[:bike_img_url],para[:bike_type],para[:bike_height],para[:description],para[:extras],para[:per_hour],para[:per_day],para[:per_week],para[:response_time],para[:last_seen],para[:count_reviews]]
  end
end